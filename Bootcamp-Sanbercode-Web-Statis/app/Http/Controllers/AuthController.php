<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('/register');
    }
    public function welcome(Request $request)
    {
        $firstname = $request['first_name'];
        $lastname = $request['last_name'];
        $male = $request['male'];
        $female = $request['female'];
        $campuran = $request['other'];
        $negara = $request['negara'];
        $Bindo = $request['Bahasa_indonesia'];
        $Bing = $request['English'];
        $lainnya = $request['others'];
        $bio = $request['bio'];

        return view('/selamatdatang', compact('firstname', 'lastname', 'male', 'female', 'campuran', 'negara', 'Bindo', 'Bing', 'lainnya', 'bio'));
    }
}