<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SELAMAT DATANG</title>
</head>

<body>
    <h1>SELAMAT DATANG {{$firstname}} {{$lastname}}</h1> <br>
    <h2>Terima kasih telah bergabung di SanberBook. Social Media kita bersama!</h2>
    <br><br>
    <h3>Data Diri</h3>
    <p>Nama: {{$firstname}} {{$lastname}}</p>
    <p>Jenis kelamin: {{$male}} {{$female}} {{$campuran}}</p>
    <p>Negara: {{$negara}}</p>
    <p>Bahas yang dikuasai: {{$Bindo}} {{$Bing}} {{$lainnya}}</p>
    <p>Bio: {{$bio}}</p>

</body>

</html>